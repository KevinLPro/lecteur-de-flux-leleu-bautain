# Flow

Flow est un projet réalisé dans le cadre du cours de python par Kévin Leleu et Roman Bautain

## Installation

prerequis : installation de pipenv

installation des dépendances 

```python
pipenv install
```

## Execution

```python
pipenv shell

./debug.sh
```

## Fonctionnement

Toutes les définition des routes du projet sont contenue dans le fichier app.py

La base de données SQLITE est généré grâce a peewee dans le fichier models.py

La définition des formulaire WTForms sont dans le fichier forms.py

Le projet contient 2 templates : 

- base_home.html qui correspond à la page d'accueil, les écrans de connexion et déconnexion 
- base.html pour les pages accessibles lorsque l'utilisateur est connecté (ecran d'accueil listant les flux de l'utilisateur, le visionnage d'un flux ainsi que l'ajout d'un nouveau flux dans la BDD) 

