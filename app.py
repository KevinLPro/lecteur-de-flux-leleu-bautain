from flask import Flask, render_template, request, redirect, flash, url_for
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user
from models import *
from forms import *
import os
import hashlib
import feedparser


app = Flask(__name__)
app.config['SESSION_TYPE'] = 'memcached'
app.config['SECRET_KEY'] = 'super secret key'

login_manager = LoginManager() 
login_manager.init_app(app)


@login_manager.user_loader
def load_user(user_id):
    return UsersList.get(id=user_id)

##Page d'accueil du site, affichage de 2 bouton pour aller sur les pages de connexion et de création d'un utilisateur
@app.route('/')
def home():
    return render_template('home.html')

##
#Formulaire de création d'un utilisateur
#Lorsque le bouton Valider est pressé, alors nous allons créer un nouvel utilisateur dans la table SQLITE UsersList 
@app.route('/inscription', methods=['GET', 'POST', ])
def inscription():
    form = Inscription()
    if form.validate_on_submit():
        pseudo = form.pseudo.data
        motDePasse = form.password.data
        motDePasse = hashlib.sha1(motDePasse.encode()).hexdigest()
        user = UsersList.select().where(UsersList.pseudo == pseudo).first()
        if(user == None):
            utilisateur = UsersList()
            utilisateur.pseudo = pseudo.upper()
            utilisateur.password = motDePasse  #Encodage du mot de passe en MD5, encoage avec un salt en SHA512 preferable mais a voir plus tard
            utilisateur.save()
            return redirect(url_for('home'))
        else:
            flash("L'utilisateur existe déja")
            return redirect(url_for('inscription'))
    return render_template('inscription.html', form=form)    

##
# Formulaire de connexion d'un utilisateur
# Lorsque le bouton validé est pressé, nous allons chercher dans la base de données si il y a un utilisateur correpondant
##
@app.route('/connexion', methods=['GET', 'POST', ])
def connexion():
    form = Connection() 
    if form.validate_on_submit():
        pseudoConnect = form.pseudo.data.upper()
        motDePasseConnect = hashlib.sha1(form.password.data.encode()).hexdigest()
        print(motDePasseConnect)
        user = UsersList.select().where((UsersList.pseudo == pseudoConnect) & (UsersList.password == motDePasseConnect)).first()
        print(user)
        if (user == None):
            print("Le pseudo ou le mot de passe ne correspond pas")
        else:
            login_user(user)
            current_user.idUtilisateur = user.id
            return redirect(url_for('accueil'))
    return render_template('connect.html', form=form)

##
# Page d'accueil de l'utilisateur connecté
# On peut y voir la liste des flux enregistrés par l'utilisateur avec le nom du flux et la description.
# On passe par le flux parsé par feedparser et la retour de la requete pour récupérer tout les liens de flux
# Pour par la suite la possibilité de supprimer un flux par un bouton
#
@app.route('/accueil')
@login_required
def accueil():
    listeFluxParse = []
    listeFluxUser = FlowList.select().where(FlowList.idUser == current_user.id)
    for item in listeFluxUser :
        fluxparse = feedparser.parse(item.lienFlux) 
        listeFluxParse.append(fluxparse)
    return render_template('accueil.html',listeFluxParse=listeFluxParse,listeFluxUser=listeFluxUser,zip=zip(listeFluxParse,listeFluxUser))

# Page d'ajout d'un nouveau flux à un utilisateur. Celui-ci devra juste renseigner l'url du flux RSS pour l'ajouter dans la base de données
@app.route('/addFlux', methods=['GET', 'POST', ])
@login_required
def addFlux():
    form = AjoutFlux()
    if form.validate_on_submit():
        monfluxBDD = FlowList.select().where((FlowList.lienFlux == form.lien.data) & (FlowList.idUser == current_user.id)).first()
        if(monfluxBDD == None): ##Si il n'y a pas d'enregistrement correpondant alors on ajoute
            flow = FlowList()
            flow.idUser = current_user.id
            flow.lienFlux = form.lien.data
            flow.save()
            flash('FLux ajouté')
            return redirect(url_for('accueil'))   
        else: ##Sinon on re-afiche la page d'ajout de flux
            return redirect(url_for('addFlux'))
    return render_template('addFlux.html',form=form)

# Route utilisées lorsque que l'utilisateur appuie sur le bouton supprimer d'un flux.
# Nous récupérons le lien du flux et le id de l'utilisateur ayant ajouté le flux
@app.route('/supprimerFlux', methods=['GET','POST'])
@login_required
def supprimerFlux():
    idFluxUser = request.args.get('idFluxUser')
    linkFlux = request.args.get('linkFlux')
    requete = FlowList.delete().where((FlowList.idUser == idFluxUser) & (FlowList.lienFlux == linkFlux))
    requete.execute()
    return redirect(url_for('accueil'))

#Route pour visionner le flux cliqué sur la page d'accueil, passage du lien du flux par parametre
@app.route('/flux', methods=['GET', 'POST'])
@login_required
def visionnerFlux():
    linkFlux = request.args.get('linkFlux')
    fluxparse = feedparser.parse(linkFlux)
    element = feedparser.parse(linkFlux).entries
    return render_template('visionnerFlux.html',fluxparse=fluxparse, element=element)

# Utilisé lorsque l'utilisateur appuie sur le bouton de deconnexion
@app.route('/deconnexion')
@login_required
def deconnexion():
    logout_user()
    return redirect(url_for('home'))

##COMMANDES CLI
@app.cli.command()
def initdb():
    create_tables()

@app.cli.command()
def dropdb():
    drop_tables()

@app.cli.command()
def addlogin():
    UsersList.create(pseudo="blanks",password="1234567890")
    UsersList.create(pseudo="Sen",password="9876543210")
    print("Users created")